#include <iostream>
#include <string>
#include <sstream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <math.h>
#include "../include/L1cache.h"
#include "../include/debug_utilities.h"

#define CHAR_SIZE 100
#define ADDR_SIZE 9
#define DISTANT 1

using namespace std;


/* -------------------- Helper funtions -------------------- */
void print_cache_config(uint16_t cache_size_kb, uint16_t associativity, uint16_t block_size_bytes, uint16_t rp)
{
  string rp_print;
  /* Replacement policy */
  switch (rp)
  {
  case LRU: rp_print = "LRU";
    break;

  case NRU: rp_print = "NRU";
    break;

  case RRIP: rp_print = "RRIP";
    break;

  default: rp_print = "Default (LRU)";
    break;
  }
  /* Print final configuration */
  string divisory_line = "____________________________________\n";

  cout << divisory_line << "Cache parameters: \n" << divisory_line << "Cache replacement policy: "<< rp_print << "\nCache Size (KB): "  << cache_size_kb << "\nCache Associativity: " << associativity << "\nCache Block Size (bytes): "<< block_size_bytes;
}

void print_statistics (float load_misses, float load_hits, float store_misses, float store_hits, int dirty_evictions)
{
  string divisory_line = "\n____________________________________\n";
  float total_misses = load_misses + store_misses;
  float total_hits = load_hits + store_hits;

  cout << divisory_line << "Simulation results: " << divisory_line << "Overall miss rate: " << total_misses/(total_hits + total_misses) << "\nRead miss rate: " << load_misses/(total_hits + total_misses) << "\nDirty evictions: " << dirty_evictions <<"\nLoad misses: " << load_misses << "\nStore misses: " << store_misses << "\nTotal misses: " << total_misses << "\nLoad hits: " << load_hits << "\nStore hits: " << store_hits << "\nTotal hits: " << total_hits << divisory_line;

  exit (0);
}

/* -------------------- Main function -------------------- */
int main(int argc, char * argv []) {
  /* Parse argruments */
  uint16_t cache_size_kb, block_size_bytes, associativity, rp;

  for (size_t i = 0; i < argc; i++)
  {
    /* Cache size */
    if (strcmp(argv[i], "-t") == 0)
    {
      cache_size_kb = atoi(argv[i+1]);
    }
    /* Line size in bytes */
    else if (strcmp(argv[i], "-l") == 0)
    {
      block_size_bytes = atoi(argv[i+1]);
    }
    /* Associativity */
    else if (strcmp(argv[i], "-a") == 0)
    {
      associativity = atoi(argv[i+1]);
    }
    /* Replacement policy */
    else if (strcmp(argv[i], "-rp") == 0)
    {
      rp = atoi(argv[i+1]);
    }
  }

  /* Get field (tag, idx, offset) size */
  int tag_size;
  int idx_size;
  int offset_size;

  field_size_get(cache_size_kb, associativity, block_size_bytes, &tag_size, &idx_size, &offset_size);

  /* New cache */
  entry **cache_blocks = cache_matrix(associativity, idx_size, rp);

  /* New operation result */
  struct operation_result result;

  /* Get trace's lines and start your simulation */
  char data_in[CHAR_SIZE];
  char address_hex[ADDR_SIZE];
  int idx, tag;
  int loadstore;
  long address;
  bool debug = false;
  float load_misses =0, load_hits=0, store_misses=0, store_hits=0;
  int dirty_evictions=0;

  while (fgets(data_in, 100, stdin) != NULL)
  {

    /* Parse line */
    if (data_in[0] == 35) // # is 35 in ASCII: no hash, no new entries
    {
      /* Get loadstore and address from entry */
      sscanf(data_in, "%*s %d %s", &loadstore, address_hex);

      /* Address from hex to decimal (char array to long) */
      stringstream ss;
      ss << hex << address_hex;
      ss >> address;
    }
    else
    {
      break;
    }

    /* Get tag and index from address */
    address_tag_idx_get(address, tag_size, idx_size, offset_size, &idx, &tag);

    /* Choose predictor */
    switch (rp)
    {
    case LRU:
      lru_replacement_policy(idx, tag, associativity, loadstore, cache_blocks[idx], &result, debug);
      break;
    case NRU:
      nru_replacement_policy(idx, tag, associativity, loadstore, cache_blocks[idx], &result, debug);
      break;
    case RRIP:
      srrip_replacement_policy(idx, tag, associativity, loadstore, cache_blocks[idx], &result, debug);
      break;

    default:
      lru_replacement_policy(idx, tag, associativity, loadstore, cache_blocks[idx], &result, debug);
      break;
    }

    /* Get statistics */
    if (result.miss_hit == HIT_LOAD)
    {
      load_hits++;
    } else if (result.miss_hit == MISS_LOAD)
    {
      load_misses++;
    }else if (result.miss_hit == HIT_STORE)
    {
      store_hits++;
    }else if (result.miss_hit == MISS_STORE)
    {
      store_misses++;
    }

    if (result.dirty_eviction)
    {
      dirty_evictions++;
    }
  }

  /* Print cache configuration */
  print_cache_config(cache_size_kb, associativity, block_size_bytes, rp);

  /* Print Statistics */
  print_statistics(load_misses, load_hits, store_misses, store_hits, dirty_evictions);

return 0;
}
